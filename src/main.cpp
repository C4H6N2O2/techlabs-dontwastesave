/*
      CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2
                    _
                   ( )
           ________//_
           \\\\\ /////
            \-------/    Coded by ~C4H6N2O2
             \_____/
             (_____)     -Fueled by many libaries-
                          * TaskScheduler         by Anatoli Arkhipenko
                          * TFT_eSPI              by Bodmer
                          * ESP8266WiFi           by Arduino
                          * ESP8266HTTPClient     by Arduino
                          * ESP8266mDNS.h         by Arduino
                          * InfluxDbClient        by Tobias Schürg
                          * NTPClient             by Fabrice Weinberg

      CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2CO2

  **************************** Absolutely no warranty ****************************

  ********* Modules ~ GoldstandardPlus ~ Badezimmer *********
  Module 1: AHT21x Sensor        [ °C (+/- 0.3°C) / %rH (+/- 2%) ]
  Module 2: ST7735 Display       [ 1.8" LCD Screen 160x128 ]
  Module 3: RGB LED              [ common cathode(-) ]
  Module 4: SR602 Sensor         [ motion sensor (0/1) ~2.5sec long interval ]
  Module 5: Push button matrix   [ 4x with resistors ]
  Module 6: BME680 Sensor        [ °C / %rH / hpA / kOhm (GAS)]

  Microcontroller: ESP8266 D1 Mini Pro [ Wifi, 16MB ]
  Coding:          PIO - VS Studio Code [ C++ ]

  ********* Build version & Changelog *********
  Build 1.0 ~ Hydrogen
  *
  * Added description via comments
  * Added changelog list
  * Added credits for the CC0 image by Evgeni Tcherkasski
  * Fixed an exception error where data is mapped with 0 as input / int-float issues
  * Added main features completed
*/
/* Changelog:


  ********* Pinout ~ All labeled as printed on the board *********
  **LEFT**
  LBL - PIN - Module
  RST - /// - ///
  A0  - A0  - Push button matrix
  D0  - 16  - ST7735 (backlight control)
  D5  - 14  - ST7735 (SCK)
  D6  - 12  - SR602 (digital signal)
  D7  - 13  - ST7735 (SDA)
  D8  - 15  - ST7735 (CS)
  3V3 - /// - (+)Terminal
  **RIGHT**
  LBL - PIN - Module
  TX  - 1   - RGB (PWM) - blue
  RX  - 3   - RGB (PWM) - green
  D1  - 5   - AHT21X (SCL) + BME680 (SCL)
  D2  - 4   - AHT21X (SDA) + BME680 (SDA)
  D3  - 0   - RGB (PWM) - red
  D4  - 2   - ST7735 (A0/DO)
  GND - /// - (-)Terminal
  5V  - /// - ///
*/
#pragma region include and define
#include <Arduino.h> //arduino library as structure
#include <credentials.h> //import passwords and api tokens !!! git ignore list
#include <icons.h> //icons for the display to push
#define _TASK_SCHEDULING_OPTIONS // support for multiple scheduling options
#include <TaskScheduler.h> //task sheduler for handling without simple timers, better perfomance, no watchdog triggering
#include <ESP8266WiFi.h>  //wifi
#include <NTPClient.h> //ntp for time and weekday management
#include <WiFiUdp.h> //wifi
#include <InfluxDbClient.h> //influxdb api for flux query of the needed data
#include <InfluxDbCloud.h> //influxdb dep.
// influxdb token and url in credentials.h
InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
#pragma region Module 2 : ST7735 Display
#include <TFT_eSPI.h> // graphics and font library for ST7735 driver chip by Bodmer
#include <SPI.h> // include spi protocol for communications, lib dep.
TFT_eSPI tft = TFT_eSPI(); // invoke library, pins defined in User_Setup.h
#define ledbg 16           // bg led = pin 16 = D0
#define GFXFF 1            // high speed rendering fonts true
//define fonts
#define FF_M09 &FreeMono9pt7b
#define FF_M12 &FreeMono12pt7b
#define FF_S09 &FreeSans9pt7b
#define FF_S12 &FreeSans12pt7b
#define CF_OL32 &Orbitron_Light_32
#define CF_OL24 &Orbitron_Light_24
#define CF_RT24 &Roboto_Thin_24
// color and design scheme 
#define TFT_CS_BG tft.color565(37, 34, 61)
#define TFT_CS_FILL tft.color565(85, 123, 131)
#define TFT_CS_FILL_END tft.color565(170, 253, 212)
#define TFT_CS_BORDER tft.color565(173, 173, 173)
#pragma endregion
#pragma endregion

#pragma region global variables
bool flag_debug = true; //debug flag, unused
int mean_forecast[7]; //array for storing the mean forecast values for each weekday
int weekday; //current weekday
String weekday_name; //current weekday name in ger with 3 letter formating
int endpoint; //endpoint amount save this day
float minute_rate; //how much is saved per minute
#pragma endregion

#pragma region callback methods prototypes
Scheduler runner; //task scheduler
void checkwifi();
void timeclient();
void read_db();
void display();
void calculations();
#pragma endregion

#pragma region task init
Task t_checkwifi(5 * TASK_SECOND, TASK_FOREVER, &checkwifi); // handle wifi (check for dc's)
Task t_timeclient(1 * TASK_SECOND, TASK_FOREVER, &timeclient); // handle time
Task t_read_db(24 * TASK_HOUR, TASK_FOREVER, &read_db); // query db and store values
Task t_calculations(3 * TASK_HOUR, TASK_FOREVER, &calculations); // calculate for visualisation
Task t_display(10 * TASK_SECOND, TASK_FOREVER, &display); // display the results
#pragma endregion

#pragma region tasks
void checkwifi()
{
  if (WiFi.status() != WL_CONNECTED) // check if WiFi is connected
  {
    WiFi.disconnect(); // disconnect
    WiFi.reconnect();  // reconnect
    unsigned long rc_start = millis();
    while ((WiFi.status() != WL_CONNECTED) && (millis() - rc_start < 25000)) // set timeout
    {
      yield();
    }
  }
}
void timeclient()
{
  timeClient.update();
}
void read_db()
{
  // Construct a Flux query
  // Query will find the worst RSSI for last hour for each connected WiFi network with this device
  String query = "from(bucket: \"foodsharing_python\") \
     |> range(start: 2022-03-26T00:00:00Z) \
     |> last() \
     |> filter(fn: (r) => r._measurement == \"forecast_week_mean\") \
     |> filter(fn: (r) => r.host == \"gcolab\")\
     ";
  // Print composed query
  Serial.print("Querying with: ");
  Serial.println(query);
  // Print ouput header
  Serial.println("==========");
  Serial.println();
  // Send query to the server and get result
  FluxQueryResult result = client.query(query);
  // Iterate over rows. Even there is just one row, next() must be called at least once.
  int i = 0;
  while (result.next())
  {
    // Get converted value for flux result column '_value' where there is RSSI value
    // Array notice: Filter by host sorts field names by alphabetical order. Therefore the structure of the array is accordingly equal to:
    // [0] = Friday
    // [1] = Monday
    // [2] = Saturday
    // [3] = Sunday
    // [4] = Thursday
    // [5] = Tuesday
    // [6] = Wednesday
    mean_forecast[i] = result.getValueByName("_value").getDouble();
    Serial.print(String(mean_forecast[i]));
    i++;
    // Get converted value for the _time column
    FluxDateTime time = result.getValueByName("_time").getDateTime();
    // Format date-time for printing
    // Format string according to http://www.cplusplus.com/reference/ctime/strftime/
    String timeStr = time.format("%F %T");
    Serial.print(" at ");
    Serial.print(timeStr);
    Serial.println();
  }
  // Check if there was an error
  if (result.getError() != "")
  {
    Serial.print("Query result error: ");
    Serial.println(result.getError());
  }
  // Close the result
  result.close();
}
void calculations()
{
  weekday = timeClient.getDay();
  //weekday = 6; //tested for SUN x,MON x,TUE x,WED x,THU x,FRI x,SAT 
  //weekday = 1;
  Serial.println("Weekday = " + String(weekday));
  switch (weekday)
  {
  case 0: // Sunday
    endpoint = mean_forecast[3];
    weekday_name = "SON";
    break;
  case 1: // Monday
    endpoint = mean_forecast[1];
    weekday_name = "MON";
    break;
  case 2: // Tuesday
    endpoint = mean_forecast[5];
    weekday_name = "DIE";
    break;
  case 3: // Wednesday
    endpoint = mean_forecast[6];
    weekday_name = "MIT";
    break;
  case 4: // Thursday
    endpoint = mean_forecast[4];
    weekday_name = "DON";
    break;
  case 5: // Friday
    endpoint = mean_forecast[0];
    weekday_name = "FRE";
    break;
  case 6: // Saturday
    endpoint = mean_forecast[2];
    weekday_name = "SAM";
    break;
  }
  // Foodsaving starts at 08:00 and ends at 22:00 therefore a total of 840 minutes, res 1min
  minute_rate = endpoint / 840.0;
  Serial.println("endpoint is " + String(endpoint));
  Serial.println("minute rate is " + String(minute_rate));
}
void display()
{
  tft.fillScreen(TFT_CS_BG);                  // reset screen
  tft.pushImage(0, 75, 160, 90, icon_castle); // draw bg image of ms city
  int time_hours = timeClient.getHours();
  int time_minutes = timeClient.getMinutes();
  if (time_hours >= 8 && time_hours <= 22) // food saving time!
  {
    if (endpoint == 0) // no food is going to be saved today... reason: Sunday?
    {
      if (weekday == 0) // sunday
      {
        tft.setFreeFont(FF_M09);
        tft.drawString("Shops closed.", 5, 22);
        int saved_weekly_amount = mean_forecast[0] + mean_forecast[1] + mean_forecast[2] + mean_forecast[3] + mean_forecast[4] + mean_forecast[5] + mean_forecast[6];
        tft.drawString(String(saved_weekly_amount) + " KG saved", 5, 42);
        tft.drawString("...this week.", 10, 57);
        tft.setFreeFont(CF_OL24);
      }
      else // unknown reason db error?
      {
        tft.setFreeFont(FF_M09);
        tft.drawString("Err. (0) ", 5, 30);
        tft.setFreeFont(CF_OL24);
      }
    }
    else //draw the visualisation of the current saving status
    {
      int minute_current = (time_hours - 8) * 60 + (time_minutes);
      float current_amount = minute_rate * minute_current;
      int current_amount_int = round(current_amount);
      Serial.println("current amount is " + String(current_amount));
      Serial.println("current amount int is " + String(current_amount_int));
      int progress_bar = map(current_amount_int, 0, endpoint, 0, 150);
      // drawing the screen
      tft.drawString(String(current_amount) + " KG", 5, 20);                       // current saving
      tft.fillRect(5, 55, 150, 25, TFT_CS_BORDER);                                  // border
      tft.fillRectHGradient(5, 55, progress_bar, 25, TFT_CS_FILL, TFT_CS_FILL_END); // progress bar
    }
  }
  else // draw something else no foodsaving at the moment
  {
    if (time_hours<8) //display last value till 00:00 then reset and display weekly stats
    {
    tft.setFreeFont(FF_M09);
    tft.drawString("Shops closed.", 5, 22);
    int saved_weekly_amount = mean_forecast[0] + mean_forecast[1] + mean_forecast[2] + mean_forecast[3] + mean_forecast[4] + mean_forecast[5] + mean_forecast[6];
    tft.drawString(String(saved_weekly_amount) + " KG saved", 5, 42);
    tft.drawString("...this week.", 10, 57);
    tft.setFreeFont(CF_OL24);
    }
  }
  // print time and weekday information
  String time_formated = timeClient.getFormattedTime(); // hh:mm:ss
  time_formated.remove(4, 3);                           // only display hh:mm
  tft.setFreeFont(FF_M09);
  tft.drawString(time_formated + " - " + weekday_name, 20, 5); // current time and weekday name in ger
  tft.setFreeFont(CF_OL24);
}
#pragma endregion

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Serial print test");
  pinMode(ledbg, OUTPUT);
  digitalWrite(ledbg, HIGH);
  tft.init();
  tft.setRotation(3);
  // tft.setFreeFont(CF_RT24);
  tft.setFreeFont(CF_OL24); //this looks better :)
  tft.setTextColor(TFT_WHITE, TFT_CS_BG);
  tft.fillScreen(TFT_CS_BG);
  tft.setSwapBytes(true); // Push images as big endian but proc thinks of small endian, therefore swapBytes

  WiFi.hostname("Foodsharing-ESP8266");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  tft.drawString("WIFI", 25, 25);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
  }
  tft.fillScreen(TFT_CS_BG);
  timeClient.begin();
  timeClient.setTimeOffset(3600); // should be right gmt+1 hour

  // Check server connection to influx
  if (client.validateConnection())
  {
    Serial.print("Connected to InfluxDB: ");
    Serial.println(client.getServerUrl());
  }
  else
  {
    Serial.print("InfluxDB connection failed: ");
    Serial.println(client.getLastErrorMessage());
  }
  runner.init();
  runner.addTask(t_timeclient); //cave: the order is important here. Dont mess around with it
  runner.addTask(t_read_db);
  runner.addTask(t_calculations);
  runner.addTask(t_display);
  runner.addTask(t_checkwifi);

  t_timeclient.setSchedulingOption(TASK_SCHEDULE_NC); // All no catchup
  t_read_db.setSchedulingOption(TASK_SCHEDULE_NC);
  t_calculations.setSchedulingOption(TASK_SCHEDULE_NC);
  t_display.setSchedulingOption(TASK_SCHEDULE_NC);
  t_checkwifi.setSchedulingOption(TASK_SCHEDULE_NC);

  t_timeclient.enable();
  t_read_db.enable();
  t_calculations.enable();
  t_display.enable();
  t_checkwifi.enable();
}

void loop()
{
  // put your main code here, to run repeatedly:
  runner.execute();
}