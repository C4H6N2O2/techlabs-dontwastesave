const Nightmare = require('nightmare');
const Xvfb = require('xvfb');
let xvfb = new Xvfb();
try {
  xvfb.startSync()
}
catch (e) {
  console.log(e)
  //xvfb.stopSync()
  process.exit(1)
}
console.log('nightmare...')
var nightmare = Nightmare({ show: false })
nightmare
  // Login page...
  .goto("https://foodsharing.de/?page=login&ref=%2F%3Fpage%3Ddashboard")  // Open login page
  .wait("#login-btn")                                                     // Await login button
  .type("#login-email", "***HIDDEN***")                       // Login forms... autotyping
  .type("#login-password", "HIDDEN")                                    // Login forms... autotyping ... WOW. Leaked password, oh no! lol
  .wait(500)                                                              // Extra time 500ms before login
  .click("#login-btn")                                                    // Login
  // Dashboard
  .wait(10000)                                                  //Await dashboard, login = complete
  // Statistic page
  .goto("https://foodsharing.de/?page=bezirk&bid=109&sub=statistic")            //Open statistic page for Münster
  //.wait("#content_top > div > div > div.user_display_name")                     //Await Data ready
  .wait(10000)                                                                   //5 seconds extra buffer, just in case
  .evaluate(() => document.querySelectorAll(".user_display_name")[1].innerText) //Extract raw data from the elemt
  // Problem here: Cant logout after evaluate, because async function(?)        //Solve this async function problem for logout (optional)?
  // .goto("https://foodsharing.de/?page=logout")
  // .wait("body.page-index")
  // .end()
  // End the scraping, hand over the scraped value with .then()
  .end()
  .then(function (aws) {
    var raw = aws;

    console.log("Extracted-RAW: " + raw);               // Mocha testing possible(!), expected raw = ""
    // Function to extract the integer from a string starting with a number...
    function ExtractUsefullSubstringLength(string1) {
      for (i = 1; i < string1.length; i++) {
        // console.log("i= ", i);
        if (!Number(string1.substring(0, i)) === false) {
          console.log(string1.substring(0, i));
        } else {
          // console.log("Returned: i=", i);
          return i - 1;
        }
      }
    }
    // Convert the RAW string to a pure number to integer (--> see function above)
    var total_kg = Number(raw.substring(0, ExtractUsefullSubstringLength(raw)));
    // Log the results
    console.log("Total KG: " + total_kg);
    // Do DB stuff here:

    const { InfluxDB } = require('@influxdata/influxdb-client')

    // You can generate an API token from the "API Tokens Tab" in the UI
    const token = 'HIDDEN='
    const org = 'Tech Labs'
    const bucket = 'foodsharing_raw'

    const client = new InfluxDB({ url: 'http://HIDDEN_local_ip', token: token })

    const { Point } = require('@influxdata/influxdb-client')
    const writeApi = client.getWriteApi(org, bucket)
    writeApi.useDefaultTags({ host: 'RbPi' })

    const point = new Point('total').floatField('kg', total_kg)
    writeApi.writePoint(point)

    writeApi
      .close()
      .then(() => {
        console.log('FINISHED')
        //xvfb.stopSync()
        process.exit(1)
      })
      .catch(e => {
        console.error(e)
        console.log('Finished ERROR')
       // xvfb.stopSync()
        process.exit(1)
      })
  })
  // Nightmare error handling... (async functions)
  .catch(error => {
    console.error('Search failed:', error)
   // xvfb.stopSync();
    process.exit(1)
  })
