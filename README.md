# Dont Waste It - Save It: Analysis of saved food per day in Münster (via foodsharing) :green_apple: - Documentation 

- [Dont Waste It - Save It: Analysis of saved food per day in Münster (via foodsharing) :green_apple: - Documentation](#dont-waste-it---save-it-analysis-of-saved-food-per-day-in-münster-via-foodsharing-green_apple---documentation)
  - [Data :bar_chart:](#data-bar_chart)
    - [Scraping :page_with_curl:](#scraping-page_with_curl)
    - [Cleaning :bookmark_tabs:](#cleaning-bookmark_tabs)
    - [Visualisation :eyes:](#visualisation-eyes)
  - [Challenges :exclamation:](#challenges-exclamation)
    - [Scraping :page_with_curl: - CSRF :cookie:](#scraping-page_with_curl---csrf-cookie)
    - [Cleaning :bookmark_tabs: - Pandas :koala:](#cleaning-bookmark_tabs---pandas-koala)
    - [Automation - Frustration :arrows_clockwise:](#automation---frustration-arrows_clockwise)
  - [Possible future features :date:](#possible-future-features-date)

## Data :bar_chart:

The analysis is focused on the data taken from foodsharing.de. 


### Scraping :page_with_curl:

The script uses Nightmare (nodejs) (with proton / electron) to connect to foodsharing.de via CSRF token-forgery (with **permission** of foodsharing.de). It requests the total amount in kg of every business in the region of Münster (via bezirks id#109). Our scraping script runs every six hours. The script parses the gathered data via the InfluxDB API for nodejs to our influxdb time based database. It therefore uses API-Tokens with precoded libraries. The first data was written to our database at ``2021-12-03``. We selected influxdb with version >= 2.0 to make use of the gui to visualise and analyse data directly via the database interface. Another benefit is that the api handling for multiple platforms is quite handy. We created multiple accounts to access the database.

*GUI snippet of our influxdb*

![](img/influxdb_gui.png)


The **structure of the dataframe** consists of the following content taken from the bucket ``foodsharing_raw``:
- `time`: of writing which is added by InfluxDB
- `_value`: which equals the total amount saved in kg
  - `_field`: which describes the content type (weight in kg)
  - `_measurement`: which describes the data type (amount)
- `host`: which is a raspberry pi in our case

The database has the following structure:
```mermaid
graph LR
    A{DB} --> B(Bucket: foodsharing_raw)
    B --> B1(host)
    B1 -->B2[Windows machine]
    B1 -->B3[Raspberry pi]
    B --> C(_measurement: total amount)
    C --> D(_field: kg)
    D --> E[_value]
        subgraph From Influx to Python
        B
        B1
        B2
        B3
        C
        D
        E
        end
```
Note: Timestamps are automatically added via InfluxDB.


### Cleaning :bookmark_tabs:

Note:
- `pd` = pandas
- `df` = dataframe

We managed to directly import the needed dataset into a pd dataframe called `raw_df` by using the influx api with a token access system. Then we started to visualise and analyse the completeness of the given date range to make sure that there are no missing values (no entries for longer than 24 hours). We determined ``2022-02-13T00:00:00Z`` as the starting point for our flux query.

![](img/0_verify_growth.png)

We then started to clean our `raw_df` by renaming and removing any unnecessary informations such as ``"result"``,``"table"``,``"_start"``,``"_stop"``. The newly created df `cleaned_df` consists of the `scrape_time` as index and the total amount saved at this time (`value_KG`). Because we want to visualise the saved food per for each weekday we need to process the data further. A quick inspection of the `cleaned_df` revealed that foodsharing.de is refreshing the dataset probably just once a day. So we decided to keep only one `value_KG` per day and dropped the rest. After that we calculated the difference between each day to reveal the amount saved per day as `food_kg_diff`. Here we started to notice a bug that we like to call `-1 bug`. On a regular sunday the shops are closed and we would expect to see 0 KG saved, instead we always seems to get -1 KG. This seems to be related to foodsharing.de handling the dataset. It is possible that this is also happening for every other day (unnoticed). We corrected that bug by subtracting -1 for each weekday. Beside adding ``food_kg_diff_corrected``, we also added `weekday_name` for each weekday to our dataframe.

A quick boxplot and histogram of the current df:

![](img/1_boxplot_weekly_savings.png)


![](img/2_histogram.png)


The ``cleaned_df_mean`` contains the mean value of the amount (`food_kg_diff_corrected`) saved per day àccording to each weekday. This df only contains 7 rows of information.  

Furthermore this freshly extracted information is written to a new bucket called ``foodsharing_python`` in the InfluxDB. We push the  ``_value`` accordingly into the measurement ``forecast_week_mean`` for each ``weekday`` as a field into the InfluxDB.

The complete database structure looks like this:
```mermaid
graph LR
    A{DB} --> B(Bucket: foodsharing_raw)
    B --> B1(host)
    B1 -->B2[Windows machine]
    B1 -->B3[Raspberry pi]
    B --> C(_measurement: total amount)
    C --> D(_field: kg)
    D --> E[_value]
    A --> F(Bucket: foodsharing_python)
        F --> G(host)
        G --> H[gcolab]
        F --> I(_measurement: forecast_week_mean)
        I --> J(_field)
        J --> J1(Monday)
        J --> J2(Tuesday)
        J --> J3(Wednesday)
        J --> J4(Thursday)
        J --> J5(Friday)
        J --> J6(Saturday)
        J --> J7(Sunday)
        J1 --> K1[_value]
        J2 --> K2[_value]
        J3 --> K3[_value]
        J4 --> K4[_value]
        J5 --> K5[_value]
        J6 --> K6[_value]
        J7 --> K7[_value]
        subgraph From Influx to Python
        B
        B1
        B2
        B3
        C
        D
        E
        end
        subgraph From Python to Influx
        F
        G
        H
        I
        J
        J1
        J2
        J3
        J4
        J5
        J6
        J7
        K1
        K2
        K3
        K4
        K5
        K6
        K7
        end
```
Note: Timestamps are automatically added via InfluxDB.


Visualisation of the data through the InfluxDB GUI:

![](img/3_data_structure_via_influxdb.png)




### Visualisation :eyes:

To visualise our generated data from the df ``cleaned_df_mean`` we decided to choose a different approach rather then creating a webpage. An ``ESP8266`` is connected to an ``ST7735`` 1.8" LCD Screen with a resolution of 160x128 pixels.
The microprocessor is programmed via `C++` using Visual Studio Code with platformio. 


The prototype breadboard with the LCD screen (red arrow) and the microprocessor (blue arrow):

![](img/visualisation/1.jpg)


Astonishing size of the microprocessor compared to a peppercorn:

![](img/visualisation/0.jpg)


Similiar to before the data is imported using the InfluxDB API with special libraries for the `Arduino framework`. To simulate a live ``ticker`` we decided to use an interpolating map function which calculates the amount of saved food  every 10 seconds and displays it accordingly. Foodsharing times are from 08:00 to 22:00. After that or on sundays a message will display that the shops are closed and the weekly sum of saved food will be shown instead. A ``progress bar`` underlines the passing time during the day.


Visualisation in action - progress bar, time, weekday and amount saved in KG at this minute:

![](img/visualisation/2.jpg)


![](img/visualisation/3.jpg)


## Challenges :exclamation:
During our journey we were facing many challenges. In particular the following were difficult to handle. 


### Scraping :page_with_curl: - CSRF :cookie:

Foodsharing.de uses ``Cross-Site-Request-Forgery`` cookies to verify a valid session id. The necessary data is only availiable after login. 

**Solution - Nightmare** :waning_crescent_moon:

Our solution is to "just" simulate a browser and the login process to handle the problem using nodejs with the library of Nightmare, which uses electron under the hood. 


### Cleaning :bookmark_tabs: - Pandas :koala:
When using a dataset with missing values for some days we encountered a rather big problem - that we like to call the `block problem`. Missing values (no values for at least 24 hours) will mess with the difference function to calculate the amount saved per day. This results in a way to high saving datapoint for a particular day, because the difference will not ignore the missing date block in between. Therefore rendering the mean forecast and the visualisation useless. Currently we do not have a working solution implented.

**Solution I - Interpolate**

We tried to fill the missing gaps by interpolating in our colab notebook ``techlabs_fs_interpolate_unstable``. The results will compromise the calculation for the mean values per weekday by disregarding special properties for each weekday. We decided to not implement this solution because it was creating "wrong" datapoints especially for the `cleaned_df_mean` df. 

The corrseponding boxplot differs greatly to the one above:

![](img/boxplot_interpolate_wrong.png)


**Solution II - Ignore**

Another solution could be to ignore the missing data blocks by creating a corresponding dataframe for each block without interruption and then calculating different ``cleaned_df_mean`` dataframes for each generated dataframe and then aggregate those to one new ``cleaned_df_mean_sum`` df. This requires ``for`` itterations while using pandas.


Flowchart of the solution:
```mermaid
flowchart TD
A{Start} --> A1[Interruption I] --> A2[Interruption II] --> A3[...] -->A4{End}
A1 --> |remove| B1[NaN Start]
B11[NaN End]
A2 --> |remove| B2[NaN Start]
B21[NaN End]
A3 --> |remove| B3[NaN Start]
B31[...]
A --- C{Start} -->B1 -->B11 --> B2 -->B21 --> B3 --> B31 -->A4
D1(Dataframe I)
D2(Dataframe II)
D3(Dataframe n ...)
subgraph raw_df
A 
A1 
A2 
A3 
A4
end
subgraph Block-I
C 
B1 
end
subgraph Block-II
B11
B2 
end
subgraph Block-n
B21
B3
end
Block-I --> |new df| D1
Block-II --> |new df|D2 
Block-n -->D3
D1 --> E1[cleaned_df_mean] --> F(Dataframe: cleaned_df_mean_sum)
D2 --> E2[cleaned_df_mean] --> F(Dataframe: cleaned_df_mean_sum)
D3 --> E3[cleaned_df_mean] --> F(Dataframe: cleaned_df_mean_sum)
F --> G[(Database)]
```
  

### Automation - Frustration :arrows_clockwise:

We implemented and automated our scraper onto a raspberry pie without gui (ubuntu server). It turns out to be quite difficult to implement ``nightmare``(js) in this way, because it needs a gui to run properly. So we started to use ``xvfb`` to simulate a screen. Since this process was not killed everytime the scraper failed to get data (thanks to async functions in nodejs) each time a new process was started. Eventually this results in a system crash and stopping the database server as well as the scraping. Now the process is killed each time via crontab before running the scraper script.
This is the reason why our database has some missing values. The solution was implemented in the process.



## Possible future features :date:

The list of possible future features is only limited by time, creativity and meaningfulness of the feature.

- **Data**
  - Improve the accuracy of the ``mean forecast`` by solving the ``block problem`` (via pandas).
  - Solving the ``-1 bug`` and / or implementing a direct api for the data from foodsharing.de (source).
- **Visualisation**
  - Simple web page with an interactive counter and informations (public relations) regarding ``foodsharing.de``.
  - Fitting the microprocessor and display from a prototype breadboard into a 3D printed enclosure.








